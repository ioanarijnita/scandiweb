import React, { useState } from "react";
import "./styles.scss";

export function ProductBox(p: {selectBoxes: () => void, code: string, name: string, price: string, size: string, weight: string, height: string, width: string, length: string}) {
    return(
        <div className = "box">
            <div style = {{marginTop: 35}}>
                <input className = "delete-checkbox" type = "checkbox" onClick = {p.selectBoxes}></input>
                <div className  = "box-content">
                <text name = "code" className = "text-style">{p.code}</text>
                <text name = "name" className = "text-style">{p.name}</text>
                <text name = "price" className = "text-style">{p.price} $ </text>
                {p.size ? <text name = "size" className = "text-style">Size: {p.size} MB</text>
                : p.weight ? <text name = "weight" className = "text-style">Weight: {p.weight} KG</text>
                : p.height ? <div>
                    <text name = "height" className = "text-style">Dimension: {p.height}x</text>
                    
                    <text name = "width" className = "text-style">{p.width}x</text>
                    
                    <text name = "length" className = "text-style">{p.length}</text>
                    </div>
                : <></>}
                </div>
            </div>
        </div>
    );
}