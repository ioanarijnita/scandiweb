import axios from 'axios';
import React, { createContext, useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { baseUrl, RerenderContext } from '../App';
import { Footer } from '../components/footer';
import { Header } from '../components/header';
import { ProductBox } from '../components/product';
import { AddProduct } from './add-product';
import './styles.scss';

interface Product {
    idProduct: string,
    sku: string,
    name: string,
    price: string,
    type: string,
    size: string,
    weight: string,
    height: string,
    width: string,
    length:string
}

function ProductList() {
    const history = useHistory();
    const { rerender } = useContext(RerenderContext)
    const [deletedData, setDeletedData] = useState(false);
    const [boxes, setBoxes] = useState<Product[]>([])
    const selectedBoxes: string[] = [];
    const obj = {
        idProduct: selectedBoxes
    }
    function selectBoxes(item: string) {
        selectedBoxes.push(item)
    }

    async function deleteBoxes() {
        await axios.post(baseUrl, obj, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
              },
        })
        .then(res => console.log(res.config.data));
        setDeletedData(!deletedData);
    }
    useEffect(() => {
        axios.get(baseUrl, {
            method: 'GET'
        })
        .then (res => {
            setBoxes(res.data)
        })
    }, [rerender, deletedData])

    useEffect(() => {
        localStorage.setItem("SKU", JSON.stringify(boxes.map(item => item.sku)));
    }, [boxes])

    return (
        <div>
            <Header 
                title = "Product List" 
                button1 = "ADD" 
                button2 = "MASS DELETE"
                isSave = {false}
                isCancel = {false}
                handleDelete = {deleteBoxes}
            />
            <div style = {{flexDirection: 'row', display: 'flex', flexWrap: 'wrap', marginRight: 20}}>
                {/* {boxes.map((item, index) => <div><p>{item.height} {item.length} {item.name} {item.price} {item.size} {item.sku} {item.type} {item.weight} {item.width} <br /> </p></div>)} */}
            {boxes.map((item, index) => <ProductBox key = {item.idProduct} code = {item.sku} name = {item.name} price = {item.price} size = {item.size} weight = {item.weight} height = {item.height} width = {item.width} length = {item.length} selectBoxes = {() => selectBoxes(item.idProduct)}></ProductBox>)}
        </div>
        <Footer />
        </div>
    );
}
export default ProductList;